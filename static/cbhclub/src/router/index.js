import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
//////////////////////////////////////////////////////////////////////////////////////////////
import home from '../components/home.vue'
import login from '../components/login.vue'
import register from '../components/register.vue'
import index from '../components/index.vue'
import car from '../components/car.vue'
import more from '../components/more.vue'
import join from '../components/join.vue'
import call from '../components/call.vue'
import info from '../components/info.vue'
import carfriend from '../components/carfriend.vue'
import weibo from '../components/weibo.vue'
//////////////////////////////////////////////////////////////////////////////////////////////
Vue.use(Router)
//////////////////////////////////////////////////////////////////////////////////////////////
const router =  new Router
({
  linkActiveClass: 'active',
  routes:
  [

    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      component: login,
      meta:
      {
        needAuth: false
      },
    },
    {
      path: '/register',
      component: register,
      meta:
      {
        needAuth: false
      },
    },
    {
      path: '/index',
      component: index,
      meta:
      {
        needAuth: false
      },
      children:
      [
        {
          path: '/home',
          component: home,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/car',
          component: car,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/more',
          component: more,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/join',
          component: join,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/call',
          component: call,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/info',
          component: info,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/carfriend',
          component: carfriend,
          meta:
          {
            needAuth: false
          }
        },
        {
          path: '/weibo',
          component: weibo,
          meta:
          {
            needAuth: false
          }
        }
      ]
    },
  ]
});
//////////////////////////////////////////////////////////////////////////////////////////////
router.beforeEach((to, from, next) => {       //路由拦截钩子
  if(!to.meta.needAuth)
  {
    next();
  }
  else {
    next();
  }
});
//////////////////////////////////////////////////////////////////////////////////////////////
export default router;
