import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import Element from 'element-ui'
import axios from 'axios'
import 'element-ui/lib/theme-chalk/index.css'
import global_ from './Global.vue'


Vue.use(Element);

Vue.prototype.GLOBAL = global_


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
