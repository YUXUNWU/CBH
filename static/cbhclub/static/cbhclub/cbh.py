#coding=utf-8
from flask import Flask, request, make_response,jsonify
from flask_cors import CORS
import string
import json
import random
import pymysql
import time

db = pymysql.connect(host="127.0.0.1", user="root", password="MYSQL2015190103", db="cbh", port=3306, use_unicode=True, charset="utf8")
cur = db.cursor()

app = Flask(__name__)
CORS(app,supports_credentials=True)
app.secret_key = 'FYlKCBmQWwPzfDI4'
mycookie = dict()

@app.route('/login',methods=['GET','POST'])
def login():
    info=request.json
    if info==None:
        return make_response(jsonify(code=405,message="非法登陆!"))
    else:
        name=info.get('name')
        password=info.get('password')
        cur.execute("SELECT * FROM account where name='%s'"%name)
        results = cur.fetchall()
        tip=0
        for result in results:
            tip=tip+1
            if tip==1:
                break
        if tip==0:
            return make_response(jsonify(code=404,message="用户名不存在，登陆失败!"))
        elif password==results[0][1]:
            mycookie[name]=name
            return make_response(jsonify(code=200,message="恭喜你，登陆成功!"))
        else:
            return make_response(jsonify(code=404,message="密码错误，登陆失败!"))


@app.route('/register',methods=['GET','POST'])
def register():
    info=request.json
    if info==None:
        return make_response(jsonify(code=405,message="非法注册!"))
    else:
        name=info.get('name')
        passwd=info.get('password')
        cur.execute("SELECT * FROM account where name='%s'"%name)
        results = cur.fetchall()
        tip=0
        for result in results:
            tip=tip+1
            if tip==1:
                break
        if tip==0:
            sql_insert = "insert into account(name,passwd) values('%s','%s');"%(name,passwd)
            sql_insert.encode('utf-8')
            cur.execute(sql_insert)
            db.commit()
            return make_response(jsonify(code=200,message="恭喜你，注册成功!"))
        else:
            return make_response(jsonify(code=405,message="该用户名已存在，请重新注册~"))

@app.route('/hot_cars', methods=['GET', 'POST'])
def hot_cars():
    info=request.json
    type=int(info.get('key'))
    if type==0:
        cur.execute("SELECT * FROM cars")
        results = cur.fetchall()
    else:
        cur.execute("SELECT * FROM cars where type='%d'"%type)
        results = cur.fetchall()
    results_list = list(results)
    results_list.sort(key=lambda x: x[2],reverse=True)
    hot_cars=[];
    num=0
    for  i in results_list:
        hot_cars.append(i)
        num=num+1
        if num==4:
            break
    return make_response(jsonify(code=200, hot_cars=hot_cars,num=num), 200)


@app.route('/all_cars', methods=['GET', 'POST'])
def all_cars():
    cur.execute("SELECT * FROM cars")
    results = cur.fetchall()
    results_list = list(results)
    results_list.sort(key=lambda x: x[2],reverse=True)
    all_cars=[]
    num=0
    for  i in results_list:
        all_cars.append(i)
        num=num+1
    return make_response(jsonify(code=200, all_cars=all_cars,num=num), 200)


@app.route('/search', methods=['GET', 'POST'])
def search():
    info=request.json
    name=info.get('car')
    cur.execute("SELECT * FROM cars where name='%s'"%name)
    results = cur.fetchall()
    tip=0
    for result in results:
        tip=tip+1
        if tip==1:
            break
    if tip==0:
        print("none")
        return make_response(jsonify(code=500,message="对不起，您搜索的车型不存在!"),200)
    else:
        car=results[0]
        return make_response(jsonify(code=200,car=car,), 200)


@app.route('/logout',methods=['GET','POST'])
def logout():
    info=request.json
    if info==None:
        return make_response(jsonify(code=401,message="您未登陆!"))
    else:
        getkey=info.get('key')
        #print mycookie
        if getkey in mycookie:
            mycookie.pop(getkey,None)
            #print mycookie
            return make_response(jsonify(code=200,message="您已成功注销!"))
        else:
            return make_response(jsonify(code=402,message="您未登陆!"))


@app.route('/myaccount',methods=['GET','POST'])
def myaccount():
    info=request.json
    if info==None:
        return make_response(jsonify(code=401,message="您未登陆!"))
    else:
        getkey=info.get('key')
        #print mycookie
        if getkey in mycookie:
            cur.execute("SELECT * FROM account where name='%s'"%getkey)
            results = cur.fetchall()
            result = list(results[0])
            myaccount = []
            myaccount.append(result[0])
            myaccount.append(result[1])
            myaccount.append(result[2])
            return make_response(jsonify(code=200,myaccount=myaccount))
        else:
            return make_response(jsonify(code=402,message="您未登陆!"))



@app.route('/write',methods=['GET','POST'])
def write():
    info=request.json
    if info==None:
        return make_response(jsonify(code=405,message="非法发帖!"))
    else:
        theme=info.get('theme')
        content=info.get('content')
        thetime=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
        accountname=info.get('accountname')
        sql_insert = "insert into weibo(theme,content,time,accountname) values('%s','%s','%s','%s');"%(theme,content,thetime,accountname)
        sql_insert.encode('utf-8')
        cur.execute(sql_insert)
        db.commit()
        return make_response(jsonify(code=200,message="恭喜你，发帖成功!"))

@app.route('/all_weibos', methods=['GET', 'POST'])
def all_weibos():
    cur.execute("SELECT * FROM weibo")
    results = cur.fetchall()
    results = list(results)
    all_weibos = []
    num=0
    for result in results:
        item = {}
        item["id"] = result[0]
        item["theme"] = result[1]
        item["accountname"] = result[4]
        item["time"] = result[3]
        all_weibos.append(item)
        num=num+1
    return make_response(jsonify(code=200, all_weibos=all_weibos,num=num), 200)

@app.route('/weibo', methods=['GET', 'POST'])
def weibos():
    info=request.json
    if info==None:
        return make_response(jsonify(code=405,message="非法访问!"))
    else:
        weiboid=int(info.get('weiboid'))
        cur.execute("SELECT * FROM weibo where id='%d'"%weiboid)
        results = cur.fetchall()
        weibo = list(results[0])
        cur.execute("SELECT * FROM comment where weiboid='%d'"%weiboid)
        comments = list(cur.fetchall())
        num=0
        for comment in comments:
            num=num+1
        print(comments)
        return make_response(jsonify(code=200, weibo=weibo,comments=comments,num=num), 200)

@app.route('/comment', methods=['GET', 'POST'])
def comment():
    info=request.json
    if info==None:
        return make_response(jsonify(code=405,message="非法访问!"))
    else:
        weiboid=int(info.get('weiboid'))
        comment=info.get('comment')
        accountname=info.get('accountname')
        thetime=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
        sql_insert = "insert into comment(content,weiboid,time,accountname) values('%s','%d','%s','%s');"%(comment,weiboid,thetime,accountname)
        sql_insert.encode('utf-8')
        cur.execute(sql_insert)
        db.commit()
        return make_response(jsonify(code=200,message="恭喜你，评论成功!"))


@app.route('/deleleweibo', methods=['GET', 'POST'])
def deleleweibo():
    info=request.json
    if info==None:
        return make_response(jsonify(code=405,message="非法发帖!"))
    else:
        deleteid=info.get('deleteid')
        sql="DELETE FROM weibo WHERE id='%d'"%(deleteid)
        cur.execute(sql)
        db.commit()
        sql2="DELETE FROM comment WHERE weiboid='%d'"%(deleteid)
        cur.execute(sql2)
        db.commit()
        return make_response(jsonify(code=200,message="您选定的帖子已删除!"))


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=7000,debug=True)
